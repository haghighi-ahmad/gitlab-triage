require 'spec_helper'

require 'gitlab/triage/entity_builders/issue_builder'

describe Gitlab::Triage::EntityBuilders::IssueBuilder do
  include_context 'network'

  let(:resources) do
    [
      { title: 'Issue #0', web_url: 'http://example.com/0' },
      { title: 'Issue #1', web_url: 'http://example.com/1' }
    ]
  end

  let(:rule) do
    {
      title: title,
      item: item,
      summary: summary
    }
  end

  let(:title) { 'Issue title for {{type}}' }
  let(:expected_title) { 'Issue title for issues' }
  let(:item) { '- [ ] [{{title}}]({{web_url}})' }
  let(:summary) { "\# {{title}}\n\nHere's the summary:\n\n{{items}}" }

  subject do
    described_class.new(
      type: 'issues',
      action: rule,
      resources: resources,
      network: network)
  end

  describe '#title' do
    it 'generates the correct issue title' do
      expect(subject.title).to eq(expected_title)
    end
  end

  describe '#description' do
    it 'generates the correct issue description' do
      expect(subject.description).to eq(<<~TEXT.chomp)
        # #{expected_title}

        Here's the summary:

        - [ ] [Issue #0](http://example.com/0)
        - [ ] [Issue #1](http://example.com/1)
      TEXT
    end

    context 'when there is no item' do
      let(:item) {}

      it 'generates a description with empty items' do
        expect(subject.description).to eq(<<~TEXT.chomp)
        # #{expected_title}

        Here's the summary:


        TEXT
      end
    end

    context 'when there is no summary' do
      let(:summary) {}

      it 'generates an empty description' do
        expect(subject.description).to be_empty
      end
    end

    context 'when resources are other issue builders' do
      let(:resources) do
        [
          described_class.new(
            type: 'issues',
            action: rule.merge(summary: 'inner summary'),
            resources: [],
            network: network)
        ]
      end

      it 'generates the description with inner issue builders' do
        expect(subject.description).to include('inner summary')
      end
    end
  end

  describe '#valid?' do
    context 'when title is nil' do
      let(:title) {}

      it 'is not valid' do
        expect(subject).not_to be_valid
      end
    end

    context 'when title is blank' do
      let(:title) { '  ' }

      it 'is not valid' do
        expect(subject).not_to be_valid
      end
    end

    context 'when title is not blank' do
      let(:title) { 'title' }

      it 'is valid' do
        expect(subject).to be_valid
      end

      context 'when there is no resources' do
        let(:resources) { [] }

        it 'is not valid' do
          expect(subject).not_to be_valid
        end
      end
    end
  end

  describe '#any_resources?' do
    context 'when there is some resources' do
      it 'returns true' do
        expect(subject).to be_any_resources
      end
    end

    context 'when there is no resources' do
      let(:resources) { [] }

      it 'returns false' do
        expect(subject).not_to be_any_resources
      end
    end
  end
end
