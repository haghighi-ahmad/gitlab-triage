require 'spec_helper'

require 'gitlab/triage/policies/rule_policy'

describe Gitlab::Triage::Policies::RulePolicy do
  include_context 'network'

  let(:type) { 'issues' }
  let(:name) { 'Policy name' }

  let(:actions) { { summarize: { item: '' } } }
  let(:policy_spec) { { name: name, actions: actions } }
  let(:resources) { [] }

  subject { described_class.new(type, policy_spec, resources, network) }

  describe '#build_issue' do
    it 'delegates to EntityBuilders::IssueBuilder' do
      expect(Gitlab::Triage::EntityBuilders::IssueBuilder)
        .to receive(:new)
        .with(
          type: type,
          action: actions[:summarize],
          resources: resources,
          network: network)
        .and_call_original

      subject.build_issue
    end
  end
end
