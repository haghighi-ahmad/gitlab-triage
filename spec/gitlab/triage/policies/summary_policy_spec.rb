require 'spec_helper'

require 'gitlab/triage/policies/summary_policy'

describe Gitlab::Triage::Policies::SummaryPolicy do
  include_context 'network'

  let(:type) { 'issues' }
  let(:name) { 'Policy name' }

  let(:actions) { { summarize: { summary: 'Grand summary: {{items}}' } } }
  let(:policy_spec) { { name: name, actions: actions } }

  let(:rule_a) do
    { name: 'Name A', actions: { summarize: { summary: 'Summary A' } } }
  end

  let(:rule_b) do
    { name: 'Name B', actions: { summarize: { summary: 'Summary B' } } }
  end

  let(:resources) do
    {
      rule_a => [{ title: name }],
      rule_b => []
    }
  end

  let(:expected_resources) do
    {
      rule_a => [{ title: name, type: type }],
      rule_b => []
    }
  end

  subject { described_class.new(type, policy_spec, resources, network) }

  describe '#resources' do
    it 'returns the resources with type attached' do
      expect(subject.resources).to eq(expected_resources)
    end
  end

  describe '#build_issue' do
    it 'contains the summary with resources but not without' do
      issue = subject.build_issue

      expect(issue.description).to include('Grand summary')
      expect(issue.description).to include('Summary A')
      expect(issue.description).not_to include('Summary B')
    end
  end
end
